#ifndef INTEGER_H
#define INTEGER_H
#include "Type.h"
#include "Helper.h"

class Integer : public Type
{
private:
	int x;
public:
	Integer(int x);
	~Integer();
	bool isPrintable() const;
	std::string toString() const;
};

#endif // INTEGER_H