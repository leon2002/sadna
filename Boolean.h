#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "Type.h"

class Boolean : public Type
{
public:
	Boolean(bool x);
	~Boolean();
	bool isPrintable() const;
	std::string toString() const;


};

#endif // BOOLEAN_H