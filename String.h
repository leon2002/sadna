#ifndef STRING_H
#define STRING_H
#include "Sequence.h"
#include <iostream>

class String : public Sequence
{
public:
	String(std::string x);
	~String();
	bool isPrintable() const;
	std::string toString() const;
};


#endif // STRING_H