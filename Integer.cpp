#include "Integer.h"

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	std::string str = "";
	int tmp = this->x;
	while (tmp != 0) {
		str += char((tmp % 10) + 48);
		tmp /= 10;
	}
	if (this->x < 0) {
		str += '-';
	}
	str = Helper::reverse(str);
	return str;
}
