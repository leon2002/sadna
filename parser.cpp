#include "parser.h"
#include <iostream>
#include <stdio.h>


Type* Parser::parseString(std::string str) throw()
{
	std::string tmp = str;
	if (str.length() > 0)
	{
		if (str[0] == ' ' || tmp[0] == '\t') {
			try {
				throw IndentationException();
			}
			catch (std::exception & ex) {
				std::cout << ex.what() << std::endl;
			}

		}
		else
			std::cout << str << std::endl;
	}

	return NULL;
}

Type* Parser::getType(std::string& str)
{
	int x;
	bool isInt = true, isBool = true, isStr = true;

	Helper::trim(str);

	isInt = Helper::isInteger(str);

	isBool = Helper::isBoolean(str);

	isStr = Helper::isString(str);
	if (isInt) {
		Helper::removeLeadingZeros(str);
		int x = stoi(str);
		Type(x);
	}




	return nullptr;
}


