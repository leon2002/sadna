#ifndef TYPE_H
#define TYPE_H
#include<iostream>

class Type
{
private:
	bool _isTemp;
public:
	Type();
	~Type();
	bool getTemp();
	void setTemp(bool isTemp);

	virtual bool isPrintable() = 0;



};





#endif //TYPE_H
