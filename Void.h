#ifndef VOID_H
#define VOID_H
#include "Type.h"

class Void : public Type
{
public:
	Void();
	~Void();
	bool isPrintable() const;
	std::string toString() const;
};

#endif // VOID_H